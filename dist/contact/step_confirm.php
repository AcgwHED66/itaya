<?php
session_start();
ob_start();
include_once(dirname(__DIR__) . '/app_config.php');
if(empty($_POST['actionFlag']) && empty($_SESSION['statusFlag'])) header('location: '.APP_URL);

$gtime = time();

//always keep this
$actionFlag       = (!empty($_POST['actionFlag'])) ? htmlspecialchars($_POST['actionFlag']) : '';
$reg_url          = (!empty($_POST['url'])) ? htmlspecialchars($_POST['url']) : '';
//end always keep this

//お問い合わせフォーム内容
$reg_type         = (!empty($_POST['type'])) ? htmlspecialchars($_POST['type']) : '';
$reg_check01      = (!empty($_POST['check01'])) ? $_POST['check01'] : array();
$reg_visit         = (!empty($_POST['visit'])) ? $_POST['visit'] : '';
$reg_consultation         = (!empty($_POST['consultation'])) ? $_POST['consultation'] : '';
$reg_inquiries         = (!empty($_POST['inquiries'])) ? $_POST['inquiries'] : '';
$reg_check01      = (!empty($_POST['check01'])) ? $_POST['check01'] : array();
$reg_name         = (!empty($_POST['nameuser'])) ? htmlspecialchars($_POST
['nameuser']) : '';
$reg_property         = (!empty($_POST['property'])) ? htmlspecialchars($_POST
['property']) : '';
$reg_chooseDate         = (!empty($_POST['chooseDate'])) ? htmlspecialchars($_POST
['chooseDate']) : '';
$reg_chooseTime         = (!empty($_POST['chooseTime'])) ? htmlspecialchars($_POST
['chooseTime']) : '';
$reg_chooseNumber1         = (!empty($_POST['chooseNumber1'])) ? htmlspecialchars($_POST
['chooseNumber1']) : '';
$reg_chooseNumber2        = (!empty($_POST['chooseNumber2'])) ? htmlspecialchars($_POST
['chooseNumber2']) : '';
$reg_furigana         = (!empty($_POST['furigana'])) ? htmlspecialchars($_POST['furigana']) : '';
$reg_company      = (!empty($_POST['company'])) ? htmlspecialchars($_POST['company']) : '';
$reg_check01      = (!empty($_POST['check01'])) ? $_POST['check01'] : array();
$reg_checkAll01   = (!empty($_POST['checkAll01'])) ? htmlspecialchars($_POST['checkAll01']) : '';
$reg_department   = (!empty($_POST['department'])) ? htmlspecialchars($_POST['department']) : '';
$reg_tel          = (!empty($_POST['tel'])) ? htmlspecialchars($_POST['tel']) : '';
$reg_fax          = (!empty($_POST['fax'])) ? htmlspecialchars($_POST['fax']) : '';
$reg_zipcode      = (!empty($_POST['zipcode'])) ? htmlspecialchars($_POST['zipcode']) : '';
$reg_address01    = (!empty($_POST['address01'])) ? htmlspecialchars($_POST['address01']) : '';
$reg_address02    = (!empty($_POST['address02'])) ? htmlspecialchars($_POST['address02']) : '';
$reg_pref_name    = (!empty($_POST['pref_name'])) ? htmlspecialchars($_POST['pref_name']) : '';
$reg_email        = (!empty($_POST['email'])) ? htmlspecialchars($_POST['email']) : '';
$reg_time         = (!empty($_POST['time'])) ? htmlspecialchars($_POST['time']) : '';
$reg_content      = (!empty($_POST['content'])) ? htmlspecialchars($_POST['content']) : '';
$br_reg_content   = nl2br($reg_content);

if($actionFlag == "confirm") {
  $thisPageName = 'contact';
  include(APP_PATH.'libs/head.php');
  $_SESSION['ses_from_step2'] = true;
  if(!isset($_SESSION['ses_gtime_step2'])) $_SESSION['ses_gtime_step2'] = $gtime;
?>
  <meta name="format-detection" content="telephone=no">
  <link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/contact.min.css">
  <!-- Anti spam part1: the contact form start -->

  <?php if(GOOGLE_RECAPTCHA_KEY_API != '' && GOOGLE_RECAPTCHA_KEY_SECRET != '') { ?>
    <script src="https://www.google.com/recaptcha/api.js?hl=ja" async defer></script>
    <script>function onSubmit(token) { document.getElementById("confirmform").submit(); }</script>
    <style>.grecaptcha-badge {display: none}</style>
  <?php } ?>

  </head>
  <body id="contact" class="contact contactCfr hasForm step02">
    <!-- HEADER -->
    <?php include(APP_PATH.'libs/header.php'); ?>
    <div id="wrap">
      <main>
        <div id="breadcrumb" class="breadcrumb">
          <div class="content">
            <ul>
              <li><span><img width="14" src="<?php echo APP_ASSETS ?>/img/common/icon/ico_breadcrumb.svg" alt="株式会社伊田屋"></span></li>
              <li><span>お問い合わせ・来場予約</span></li>
            </ul>
          </div>
        </div>
        <div class="visual">
          <div class="content">
            <span>Contact</span>
            <h1>お問い合わせ・来場予約</h1>
          </div>
        </div>
        <form method="post" class="confirmform" action="../complete/?g=<?php echo $gtime ?>" name="confirmform" id="confirmform">
          <div class="formBlock container">
            <div class="stepImg">
              <img src="<?php echo APP_ASSETS; ?>img/contact/img_step02.png" width="500" height="77" alt="STEP2 入力" />
            </div>
            <div>
              <p class="hid_url">Leave this empty: <input type="text" name="url" value="<?php echo $reg_url ?>"></p><!-- Anti spam part1: the contact form -->
              <table class="tableContact" cellspacing="0">
                <tr>
                  <th>ご用件</th>
                  <td><?php echo $reg_type; ?></td>
                </tr>
                <?php if($reg_property) : ?>
                <tr>
                  <th>物件名</th>
                  <td><?php echo $reg_property; ?></td>
                </tr>
                <?php endif; ?>
                <?php if($reg_chooseDate || $reg_chooseTime) : ?>
                <tr>
                  <th>希望来場日時</th>
                  <td>
                    <?php echo $reg_chooseDate.' <i style="padding: 0 2px;"></i> '.$reg_chooseTime  ?>
                  </td>
                </tr>
                <?php endif; ?>
                <?php if($reg_chooseNumber1 || $reg_chooseNumber2) : ?>
                <tr>
                  <th>参加人数</th>
                  <td>大人: <?php echo $reg_chooseNumber1.'人 / '. '子供: '.$reg_chooseNumber2.'人';  ?></td>
                </tr>
                <?php endif; ?>
                <tr>
                  <th>お名前</th>
                  <td><?php echo $reg_name ?></td>
                </tr>
                <?php if($reg_furigana) : ?>
                    <tr>
                      <th>ふりがな</th>
                      <td>
                        <?php echo $reg_furigana; ?>
                      </td>
                    </tr>
                    <?php endif; ?>
                <tr>
                  <th>電話番号</th>
                  <td><?php echo $reg_tel ?></td>
                </tr>
                <?php if($reg_zipcode ) : ?>
                    <tr>
                      <th>郵便番号</th>
                      <td>
                        <?php echo '〒'.$reg_zipcode ; ?>    
                      </td>
                    </tr>
                    <?php endif; ?>
                <tr>
                  <th>住所</th>
                  <td><?php echo $reg_address01 ?></td>
                </tr>
                <tr>
                  <th>メールアドレス</th>
                  <td><?php echo $reg_email ?></td>
                </tr>
                <?php if($reg_content) : ?>
                    <tr>
                      <th>備考欄</th>
                      <td>
                        <?php echo $br_reg_content ; ?>
                      </td>
                    </tr>
                    <?php endif; ?>
              </table>
            </div>


            <input type="hidden" name="type" value="<?php echo $reg_type  ?>">
            <input type="hidden" name="nameuser" value="<?php echo $reg_name ?>">
            <input type="hidden" name="chooseDate" value="<?php echo $reg_chooseDate ?>">
            <input type="hidden" name="chooseTime" value="<?php echo $reg_chooseTime ?>">
            <input type="hidden" name="chooseNumber1" value="<?php echo $reg_chooseNumber1 ?>">
            <input type="hidden" name="chooseNumber2" value="<?php echo $reg_chooseNumber2 ?>">
            <input type="hidden" name="furigana" value="<?php echo $reg_furigana ?>">
            <input type="hidden" name="company" value="<?php echo $reg_company ?>">
            <input type="hidden" name="department" value="<?php echo $reg_department ?>">
            <input type="hidden" name="tel" value="<?php echo $reg_tel ?>">
            <input type="hidden" name="fax" value="<?php echo $reg_fax ?>">
            <input type="hidden" name="email" value="<?php echo $reg_email ?>">
            <input type="hidden" name="zipcode" value="<?php echo $reg_zipcode ?>">
            <input type="hidden" name="pref_name" value="<?php echo $reg_pref_name ?>">
            <input type="hidden" name="address01" value="<?php echo $reg_address01 ?>">
            <input type="hidden" name="content" value="<?php echo $reg_content ?>">
 

            <p class="taR"><a href="javascript:history.back()">入力内容を修正する</a></p>
            <p class="taC t20b20">
              <?php if(GOOGLE_RECAPTCHA_KEY_API != '') { ?>
                <button name="actionFlag" value="send" class="g-recaptcha" data-size="invisible" data-sitekey="<?php echo GOOGLE_RECAPTCHA_KEY_API ?>" data-callback="onSubmit"><span>この内容で送信する</span></button>
              <?php } else { ?>
                <button id="btnSend"><span>この内容で送信する</span></button>
              <?php } ?>
              <input type="hidden" name="actionFlag" value="send">
            </p>
            <p class="taC t30b0">上記フォームに送信できない場合は、必要項目をご記入の上、○○○@○○○○までメールをお送りください。<br>
          </div>
        </form>
      </main>
    </div>
    <!-- FOOTER -->
    <?php include(APP_PATH.'libs/footer.php'); ?>
    <script>
      // $(document).ready(function() {
      //   $('#confirmform').on('click','#btnSend',function(e){
      //     e.preventDefault();
      //     $(this).html('<span>送信中...</span>').prop('disabled',true).addClass('disabled');
      //     $('#confirmform').submit();
      //   })
      // });
      $(document).ready(function() {
        if($('#mailContact').length) {
          var address = '○○○' + '@' + '○○○○';
          $('#mailContact').attr('href', 'mailto:' + address).text(address);
        }
      });
    </script>
  </body>
  </html>
<?php } ?>