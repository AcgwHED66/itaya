<?php
session_start();
header("Cache-control: public");
ob_start();
include_once(dirname(__DIR__) . '/app_config.php');
$thisPageName = 'contact';
include(APP_PATH.'libs/head.php');
?>
<meta http-equiv="expires" content="86400">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/contact.min.css">
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/form/validationEngine.jquery.css">
</head>
<body id="contact" class="contact hasForm">
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap" class="">
  <main>
    <div id="breadcrumb" class="breadcrumb">
      <div class="content">
        <ul>
          <li><span><img width="14" src="<?php echo APP_ASSETS ?>/img/common/icon/ico_breadcrumb.svg" alt="株式会社伊田屋"></span></li>
          <li><span>お問い合わせ・来場予約</span></li>
        </ul>
      </div>
    </div>
    <div class="visual">
      <div class="content">
        <span>Contact</span>
        <h1>お問い合わせ・来場予約</h1>
      </div>
    </div>
    <section class="contact-box">
      <div class="contact-inner content">
        <div class="contact-area">
          <div class="contact-left">
            <h2 class="contact-ttl">事前の来場予約で<span>QUOカード500円分</span><span>プレゼント!</span></h2>
            <p class="contact-txt">※各物件ページでお問い合わせいただいた場合も、プレゼントします。</p>
          </div>
          <div class="contact-right">
            <img src="<?php echo APP_ASSETS ?>img/contact/img_heading.png" alt="事前の来場予約でQUOカード500円分プレゼント!">
          </div>
        </div>
        <p class="contact-bottom">さらに、新分譲地・新モデルハウス・イベント・キャンペーン情報など、<br>伊田屋のお得情報も、いち早くお届けします!<span>※情報が不要な場合は備考欄にご記入ください。</span></p>
      </div>
      <div class="contact-infor"> 
        <p class="content"><span>電話で問い合わせる</span><span>0120-47-3441</span> <span>受付時間 : 00:00〜00:00 水曜定休</span></p>
      </div>
    </section>
    <form method="post" class="contactform" id="contactform" action="confirm/?g=<?php echo time() ?>" name="contactform" onSubmit="return check()">
      <div class="formBlock container">
        <p class="txtContact">
          <span>Contact</span>お問い合わせ
        </p>
        <div class="stepImg">
          <img src="<?php echo APP_ASSETS; ?>img/contact/img_step01.svg" width="500" height="77" alt="STEP1 入力" class="pc" />
          <img src="<?php echo APP_ASSETS; ?>img/contact/img_step01SP.png" width="335" height="45" alt="STEP1 入力" class="sp" />
        </div>
        <p class="hid_url">Leave this empty: <input type="text" name="url"></p><!-- Anti spam part1: the contact form -->
        <table class="tableContact">
          <tr>
            <th><span>必須</span>ご用件</th>
            <td>
              <label class="wrap-form">
                <input type="radio" name="type" value="来場予約" data-check="input-wrap" class="validate[required]">
                <em class="checkmark"></em>
                <span class="fz14">来場予約</span></label>
              <label class="wrap-form">
                <input type="radio" name="type" value="ご相談" class="validate[required]">
                <em class="checkmark"></em>
                <span class="fz14">ご相談</span></label>
              <label class="wrap-form">
                <input type="radio" name="type" value="その他お問い合わせ" class="validate[required]">
                <em class="checkmark"></em>
                <span class="fz14">その他お問い合わせ</span>
              </label>
            </td>
          </tr>
          <!-- wrapgroup -->
          <tr class="input-wrap">
            <th><span>必須</span>物件名</th>
            <td>
              <div class="input-select">
                <select name="property" class="validate[required]">
                  <option value="" disabled selected>選択してください</option>
                  <option value="選択してください 01">選択してください 01</option>
                  <option value="選択してください 02">選択してください 02</option>
                  <option value="選択してください 03">選択してください 03</option>
                  <option value="選択してください 04">選択してください 04</option>
                  <option value="選択してください 05">選択してください 05</option>
                </select>
              </div>
            </td>
          </tr>
          <tr class="input-wrap input-wrap--child">
            <th><span>必須</span>希望来場日時</th>
            <td>
              <div class="input-select input-click" onclick="jsFunction();">
                <div class="input-width">
                  <input type="text" readonly name="chooseDate" class="day_input validate[required]" placeholder="日付を選択" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
                  <input type="hidden" name="date1" id="date1" class="date_input_return">
                  <input type="text" name="date1_temp" class="date_input_return_hide">
                  <input type="text" name="date1_hide"  placeholder="日付を選択"  class="date_input_hide validate[required]">
                </div>
              </div>
              <div class="input-select">
                <div class="input-width">
                  <select name="chooseTime" class="validate[required]">
                    <option value="" disabled selected>時間を選択</option>
                    <option value="10:00〜">10:00〜</option>
                    <option value="10:30〜">10:30〜</option>
                    <option value="11:00〜">11:00〜</option>
                    <option value="11:30〜">11:30〜</option>
                    <option value="12:00〜">12:00〜</option>
                    <option value="12:30〜">12:30〜</option>
                    <option value="13:00〜">13:00〜</option>
                    <option value="13:30〜">13:30〜</option>
                    <option value="14:00〜">14:00〜</option>
                    <option value="14:30〜">14:30〜</option>
                    <option value="15:00〜">15:00〜</option>
                    <option value="15:30〜">15:30〜</option>
                    <option value="16:00〜">16:00〜</option>
                    <option value="16:30〜">16:30〜</option>
                    <option value="17:00〜">17:00〜</option>
                  </select>
                </div>
              </div>
            </td>
          </tr>
          <tr class="input-wrap input-wrap--inner">
            <th><span>必須</span>希望来場日時</th>
            <td>
              <div class="input-select">
                <span>大人</span>
                <div class="input-width input-width--type1">
                  <select name="chooseNumber1" class="validate[required]">
                    <option value="" disabled selected>--</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                </div>
                <span>人</span>
              </div>
              <div class="input-select">
                <span>子供</span>
                <div class="input-width input-width--type1">
                  <select name="chooseNumber2" class="validate[required]">
                    <option value="" disabled selected>--</option>
                    <option value="1">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                </div>
                <span>人</span>
              </div>
            </td>
          </tr>
          <!-- end wrapgroup -->
          <tr>
            <th><span>必須</span>お名前</th>
            <td><span>例 ) 山田 太郎</span><input type="text" name="nameuser" id="nameuser" class="validate[required]"></td>
          </tr>
          <tr>
            <th><span class="non">任意</span>ふりがな</th>
            <td>
              <span>例 ) やまだ たろう</span>
              <input type="text" name="furigana" id="furigana" class="validate[custom[furigana]]">
            </td>
          </tr>
          <tr>
            <th><span>必須</span>電話番号</th>
            <td><span>例 ) 012-3456-7890</span>
            <input type="tel"  name="tel" id="tel" class="validate[required,custom[phone]] frmrequired">
            </td>
          </tr>
          <tr class="wd">
            <th><span class="non">任意</span>郵便番号</th>
            <td><span>例 ) 012-3456</span><span>〒</span>
            <input type="text" name="zipcode" id="zipcode" onKeyUp="AjaxZip3.zip2addr(this,'','address01','address01')" class="validate[custom[zipcode]] txtzip">
          </td>
          </tr>
          <tr>
            <th><span>必須</span>住所</th>
            <td><span>例 ) 愛知県名古屋市中区○○0丁目 1-2</span><input type="text" name="address01" id="address01" class="validate[required]"></td>
          </tr>
          <tr>
            <th><span>必須</span>メールアドレス</th>
            <td><span>例 ) sample@info.com</span><input type="text" name="email" id="email" class="validate[required,custom[email]]"></td>
          </tr>
          <tr>
            <th><span class="non">任意</span>備考欄</th>
            <td><textarea name="content" id="content" class=""></textarea></td>
          </tr>
        </table>
        <div class="txtContact01">
          <p class="t0b10">【個人情報の取扱いについて】</p>
          <ul class="t0b20">
            <li>本フォームからお客様が記入・登録された個人情報は、資料送付・電子メール送信・電話連絡などの目的で利用・保管し、第三者に開示・提供することはありません。詳しくは、<a href="<?php echo APP_URL ?>" target="_blank">プライバシーポリシー</a> をご覧下さい。</li>
          </ul>
        </div>
        <div class="taC">
          <p class="checkAgree">
            <label>
                <span class="grBtn">
                  <input type="checkbox" name="check1" id="check1" value="ok">
                  <em class="checkmark"></em>
                </span>
              <span class="fz14"> 個人情報の取り扱いに同意する</span>
            </label>
          </p>
          <p class="t30b20">
            <button id="btnConfirm" class="btnConfirm"><span>入力内容を確認する</span></button>
            <input type="hidden" name="actionFlag" value="confirm">
          </p>
        </div>
        <p class="taC t30b0 txtContact01">上記フォームに送信できない場合は、必要項目をご記入の上、○○○@○○○○までメールをお送りください。</p><!-- Anti spam part2: clickable email address -->
      </div>
    </form>
  </main>
</div>
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
<script src="<?php echo APP_ASSETS; ?>js/jquery1-12-4.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/form/ajaxzip3.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/form/jquery.validationEngine.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/form/languages/jquery.validationEngine-ja.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/jquery-ui.min.js"></script>
<script>
  $(document).ready(function(){
    $('#contactform').validationEngine({
      promptPosition: 'topLeft',
      scrollOffset: ($('.header').outerHeight() + 5),
    });
    window.onbeforeunload = function(){
      if(document.contactform.check1 && document.contactform.check1.checked) {
        $('html, body').scrollTop($('#contactform').offset().top);
      }
    };
  })
  function check(){
    if(document.contactform.check1 && !document.contactform.check1.checked){
      window.alert('「個人情報の取扱いに同意する」にチェックを入れて下さい');
      return false;
    }
  }
  $(document).ready(function() {
    if($('#mailContact').length) {
      var address = '○○○' + '@' + '○○○○.com';
      $('#mailContact').attr('href', 'mailto:' + address).text(address);
    }
  });
  // btn confirm
  // datepicker
  function calldatepicker() {
    //  var current_year = "2021";
    $(".day_input").datepicker({
      minDate: 0,
      dateFormat: 'yy年m月d日',
      // beforeShowDay: function(date){
      //   var disabledDays = [];
      //   var string = jQuery.datepicker.formatDate('mm/dd', date);
      // },
      onSelect: function(dateString, obj){
        var _this = $(this);
        _this.siblings('.date_input_hide').val(_this.val()).blur();
        var strDay = obj.selectedDay;
        var strMonth = obj.selectedMonth + 1;
        var strYear = obj.selectedYear;
        if(strDay < 10) strDay = '0' + strDay;
        var fullReturnDate = '' + strYear + strMonth + strDay;
        _this.next('input[type="hidden"]').val(fullReturnDate);
        $(this).siblings('.date_input_return_hide').val($(this).siblings(".date_input_return").val());
      },
    })
  }
  $(window).on('load', function() {
    $(".input-width .date_input").each(function() {
      $(this).val($(this).siblings(".date_input_hide").val());
    });
    $(".input-width .date_input_return").each(function() {
      $(this).val($(this).siblings(".date_input_return_hide").val());
    });
  });
  calldatepicker();
  function jsFunction() {
    $(".input-click").datepicker("show");
  }
  // input-select
  function changeForm(){
    $(".input-wrap").hide().find("input, textarea, select").prop('disabled', true);
    $('input[name="type"]').on('change', function(){
      $(".input-wrap").hide().find("input, textarea, select").prop('disabled', true);
      $(".input-wrap").find('input').removeClass('validate[required]');
      var inputType = $(this).filter(':checked').data('check');
      $('.'+inputType).show().css('opacity','1').find("input, textarea, select").prop('disabled', false);
      $('.'+inputType).find('input[name="type"]').addClass('validate[required]');
    })
    $('input[name="type"]').each(function(){
      if($(this).is(':checked')){
        var inputType = $(this).data('check');
        $('.'+inputType).show().css('opacity','1').find("input, textarea, select").prop('disabled', false);
      }
    })
  }
  function remove_item() {
    $('input[name="input-wrap"]').on('change', function(){
      if($(this).val() != '来場予約') {
        $(".input-wrap").hide().find("input, textarea, select").prop('disabled', true).removeAttr('checked');
      }
    })
  }
  $(window).on('pageshow', function(){
    if($("#check1").is(":checked")) {
      $('#btnConfirm').prop('disabled', false);
    } else {
      $('#btnConfirm').prop('disabled', true);
    }
  })
 
  $("#check1").click(function(){
    if($(this).is(":checked")) {
        $('#btnConfirm').prop('disabled', false);
    } else {
        $('#btnConfirm').prop('disabled', true);
    }
  })
  $(window).on('pageshow', function(){
    changeForm();
    remove_item();
  });
</script>
</body>
</html>
