<!-- Google Tag Manager -->
<!-- End Google Tag Manager -->
<header class="header">
  <div class="inHeader">
    <h1 id="logo">
      <a href="<?php echo APP_URL ?>">
        <img src="<?php echo APP_ASSETS ?>img/common/logo.svg" alt="株式会社伊田屋" class="pc">
        <img src="<?php echo APP_ASSETS ?>img/common/logo.png" alt="株式会社伊田屋" class="sp">
      </a>
    </h1>
    <p class="hamberger">
      <span class="ham"></span>
      <span class="ber"></span>
      <span class="ger"></span>
      <small>メニュー</small>
    </p>
    <div class="layerMenu">
      <div class="inLayer">
        <div class="hNavi">
          <ul>
            <li class="calling">
              <a class="tel" href="tel:+0120473441"><span>お電話</span>
                <div class="phoneBox">
                  <div class="phoneBox__icon">
                    <img width="18" height="33" src="<?php echo APP_ASSETS ?>img/common/icon/ico_tel.svg" alt="お電話でのお問い合わせ">
                    <span>お電話でのお問い合わせ</span>
                  </div>
                  <div class="phoneBox__num">
                    <p>0120-47-3441</p>
                    <span>00:00〜00:00 水曜定休</span>
                  </div>
                </div>
              </a>
            </li>
            <li class="mailing"><a href="<?php echo APP_URL ?>contact/"><span class="note">お問い合わせ来場予約</span></a></li>
          </ul>
        </div>
        <ul class="gNavi">
          <li class="sp">
            <div>
              <p class="gLabel">HOME</p>
              <div class="gList">
                <p><a href="<?php echo APP_URL ?>">分譲住宅を<br>お考えの方</a></p>
                <p><a href="<?php echo APP_URL ?>">注文住宅を<br>お考えの方</a></p>
                <p><a href="<?php echo APP_URL ?>">リフォームを<br>お考えの方</a></p>
              </div>
            </div>
          </li>
          <li class="sp">
            <p class="gLabel"><a href="<?php echo APP_URL ?>">伊田屋の家づくり</a></p>
            <p class="gLabel"><a href="<?php echo APP_URL ?>">施工実績</a></p>
            <p class="gLabel"><a href="<?php echo APP_URL ?>">お客様の声</a></p>
            <p class="gLabel"><a href="<?php echo APP_URL ?>">イベント</a></p>
            <p class="gLabel"><a href="<?php echo APP_URL ?>">物件情報</a></p>
            <p class="gLabel"><a href="<?php echo APP_URL ?>">企業情報</a></p>
            <p class="gLabel"><a href="<?php echo APP_URL ?>">トピックス</a></p>
          </li>
          <li class="gHome pc"><a href="<?php echo APP_URL ?>subdivision/"><span>分譲住宅</span></a></li>
          <li class="gMake pc"><a href="<?php echo APP_URL ?>about/"><span>伊田屋の家づくり</span></a></li>
          <li class="gInfor pc"><a href="<?php echo APP_URL ?>estates/"><span>物件情報</span></a></li>
          <li class="gResult pc"><a href="<?php echo APP_URL ?>works/"><span>施工実績</span></a></li>
          <li class="gVoice pc"><a href="<?php echo APP_URL ?>reviews/"><span>お客様の声</span></a></li>
          <li class="gEvent pc"><a href="<?php echo APP_URL ?>events/"><span>イベント</span></a></li>
          <li class="gMail sp"><a href="<?php echo APP_URL ?>">お問い合わせ・来場予約</a></li>
          <li class="gSocial sp">
            <div class="col2">
              <p class="text1"><a href="https://www.facebook.com/itaya.gifu/" target=“_blank” rel=“noopener” class="opa" style="opacity: 1;">facebook</a></p>
              <p class="text2"><a href="https://www.insstagram.com/itaya.gifu/" target=“_blank” rel=“noopener” class="opa" style="opacity: 1;">instagram</a></p>
            </div>
          </li>
          <li class="gLAst sp"><a href="<?php echo APP_URL ?>privacy-policy/">プライバシーポリシー</a></li>
        </ul>
        <p class="close_layer"><span>× 閉じる</span></p>
      </div>
    </div>
  </div>
</header>
