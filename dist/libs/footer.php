<footer id="footer">
  <div class="footerContact">
    <img class="PC" src="<?php echo APP_ASSETS ?>img/common/other/img_footer_contact.jpg" alt="株式会社伊田屋">
    <img class="SP" src="<?php echo APP_ASSETS ?>img/common/other/img_footer_contact_mb.jpg" alt="株式会社伊田屋">
    <div class="footerContent">
      <p class="title">CONTACT<span>ご相談・お問合せ<br class="SP">は無料です</span></p>
      <p class="subtitle">伊田屋の家づくりに関する<br>ご質問・ご相談などお気軽にお問い合わせください</p>
      <div class="footerFlex">
        <ul>
          <li class="left">
            <a  href="tel:+0120473441">
              <p class="phone">電話でのお問合わせ</p>
              <p class="infor">0120-47-3441 <span>00:00〜00:00 水曜定休</span></p>
          </a>
          </li>
          <li class="right">
            <a href="<?php echo APP_URL ?>">
              <p class="mail">フォームからお問合せ</p>
              <p class="text">お問い合わせ・来場予約</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footerLogo">
    <a href="javascript:void(0)" rel="<?php echo APP_URL ?>">
      <img src="<?php echo APP_ASSETS ?>img/common/logo.svg" width="267" height="49" alt="株式会社伊田屋">
    </a>
  </div>
  <div class="footerInner clearfix">
    <ul class="col1">
      <li><a href="<?php echo APP_URL ?>">HOME</a></li> 
      <li><a href="<?php echo APP_URL ?>">施工実績</a></li> 
      <li><a href="<?php echo APP_URL ?>">企業情報</a></li>
      <li><a href="<?php echo APP_URL ?>">伊田屋の家づくり</a></li>
      <li><a href="<?php echo APP_URL ?>">お客様の声</a></li>
      <li><a href="<?php echo APP_URL ?>">トピックス</a></li>
      <li><a href="<?php echo APP_URL ?>">分譲住宅をお考えの方</a></li>
      <li><a href="<?php echo APP_URL ?>">物件情報</a></li>
      <li><a href="<?php echo APP_URL ?>">お問い合わせ・来場予約</a></li>
      <li><a href="<?php echo APP_URL ?>"></a></li>
      <li><a href="<?php echo APP_URL ?>">イベント・見学会</a></li>
      <li><a href="<?php echo APP_URL ?>">プライバシーポリシー</a></li>
    </ul>
    <div class="col2">
      <p class="text1"><a href="https://www.facebook.com/itaya.gifu/" class="opa">facebook</a></p>
      <p class="text2"><a href="https://www.insstagram.com/itaya.gifu/" class="opa">instagram</a></p>
    </div>
  </div>
  <div class="copyright"><span>© 2020 株式会社伊田屋</span></div>
</footer>

<script src="<?php echo APP_ASSETS; ?>js/common.min.js"></script>
<script>
  var list = document.querySelectorAll("div[data-src]");
  for (var i = 0; i < list.length; i++) {
    var url = list[i].getAttribute('data-src');
    list[i].style.backgroundImage="url('" + url + "')";
  }
</script>