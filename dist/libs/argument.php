<?php
$pagename = str_replace(array('/', '.php'), '', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
$pagename = $pagename ? $pagename : 'default';
$pagename = (isset($thisPageName)) ? $thisPageName : $pagename;
switch ($pagename) {
  case 'top':
    if(empty($titlepage)) $titlepage = 'Itaya Index | 株式会社伊田屋';
    if(empty($desPage)) $desPage = '';
    if(empty($keyPage)) $keyPage = '';
    if(empty($txtH1)) $txtH1 = 'H1 content for Itaya Index';
  break;
  case 'topics':
    if(empty($titlepage)) $titlepage = 'Itaya Topics | 株式会社伊田屋';
    if(empty($desPage)) $desPage = '';
    if(empty($keyPage)) $keyPage = '';
    if(empty($txtH1)) $txtH1 = 'H1 content for Itaya Topics';
  break;
  case 'contact':
    if(empty($titlepage)) $titlepage = 'Itaya Contact | 株式会社伊田屋';
    if(empty($desPage)) $desPage = '';
    if(empty($keyPage)) $keyPage = '';
    if(empty($txtH1)) $txtH1 = 'H1 content for Itaya Contact';
  break;
  default:
    if(empty($titlepage)) $titlepage = '404 Not Found';
    if(empty($desPage)) $desPage = '';
    if(empty($keyPage)) $keyPage = '';
    if(empty($txtH1)) $txtH1 = '';
}