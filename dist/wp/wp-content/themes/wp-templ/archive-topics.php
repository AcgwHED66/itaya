<?php
$thisPageName = 'topics';
$param_topics_cat = array(
  'post_type'   => 'topics',
  'parent'      => 0,
  'hide_empty'  => true,
  'taxonomy'    => 'topicscat',
);
$categories_topic = get_categories($param_topics_cat);

if(is_tax()){
  $current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
  $current_slug = $current_term->slug;
}
// $curr_monthnum = get_query_var('monthnum') ? get_query_var('monthnum') : '';
$curr_year = get_query_var('year') ? get_query_var('year') : '';

include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/topics.min.css">
</head>
<body id="topics" class="archive topics">
  <!-- HEADER -->
  <?php include(APP_PATH.'libs/header.php'); ?>
<main id="container clearfix">
    <!-- /container start -->
    <div id="breadcrumb" class="breadcrumb">
      <div class="content">
        <ul>
          <li><a href="<?php echo APP_URL ?>"><img width="14" src="<?php echo APP_ASSETS ?>/img/common/icon/ico_breadcrumb.svg"></a></li>
          <li><span>トピックス</span></li>
        </ul>
      </div>
    </div>
    <div class="visual">
      <div class="content">
        <span>Topics</span>
        <h1>トピックス</h1>
      </div>
    </div>
    <div class="topics-inner">
      <div class="topics-area">
        <!-- list article post -->
        <div class="c-article">
            <ul class="c-article__list">
            <?php
              $topics = new WP_Query();
              $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
              $year = get_the_time('Y');
              if (is_year()) {
                $year = get_the_time('Y');
                $param = array(
                  'post_type'=>'topics',
                  'order' => 'DESC',
                  'post_status' => 'publish',
                  'posts_per_page' => '10',
                  'year' => $year,
                  'paged' => $paged
                );
              } else {          
                $param=array(
                  'post_type'=>'topics',
                  'order' => 'DESC',
                  'post_status' => 'publish',
                  'posts_per_page' => '10',
                  'paged' => $paged
                );
              }
              if(isset($current_slug)) {
                $param['tax_query'] = array(
                  array(
                    'taxonomy'     => "topicscat",
                    'field'        => 'slug',
                    'terms'        => $current_slug
                  )
                );
              }
              $topics->query($param);
              if ($topics->have_posts()) : 
                while ($topics->have_posts()) : $topics->the_post();
                $singleID = get_the_id();
                $get_field =  get_fields();
                $term = get_the_terms($singleID, 'topicscat');
            ?>
              <?php while ( have_rows( 'topics_detail' ) ) : the_row(); ?>
              <li class="c-article__item">
                <a href="<?php the_permalink(); ?>">
                  <?php
                      $imageThumb= get_sub_field('topics_image');
                      if ( $imageThumb ) { ?>
                      <?php $thumb =  $imageThumb; ?>
                      <?php } else { ?>
                      <?php $thumb =  APP_ASSETS.'img/top/img_dummy_bg.png' ?>
                    <?php } ?>
                  <div class="c-article__img" data-src="<?php echo $thumb; ?>"></div>
                  <div class="c-article__info">
                    <div class="c-article__time">
                      <span><?php echo get_the_time('Y.m.d'); ?></span>
                      <?php if(isset($term) && $term) :
                        foreach($term as $itemcat) :
                      ?>
                      <p><?php echo $itemcat->name; ?></p>
                      <?php endforeach; endif; ?>
                    </div>
                    <div class="c-article__cont">
                      <h2 class="c-article__ttl"><?php echo get_the_title();?></h2>
                      <div class="c-article__txt"><?php the_content();?></div>
                    </div>
                  </div>
                </a>
              </li>
              <?php endwhile; ?>
              <?php endwhile;?>
            </ul>
            <?php  endif; ?>
            <?php if(function_exists('wp_pagenavi')): ?>
            <div class="pagenavi sp">
                <?php wp_pagenavi(array('query' => $topics)); ?>
            </div>
            <?php endif; ?>

        </div>
        <!-- end list article post -->
      </div>
      <!-- side-topics -->
      <?php include('sidebar-topics.php'); ?>
      <!-- end side-topics -->
    </div>
    <?php if(function_exists('wp_pagenavi')): ?>
     <div class="pagenavi pc">
        <?php wp_pagenavi(array('query' => $topics)); ?>
    </div>
    <?php endif; ?>
  </div>
    <!-- /container end -->
</main>
  <!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
<script src="<?php echo APP_ASSETS ?>js/page/topics.min.js"></script>
</body>
</html>