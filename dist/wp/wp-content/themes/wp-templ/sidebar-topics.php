<div class="c-sidebar">
  <div class="c-sidebar__category">
    <p class="c-sidebar__heading">Category</p>
    <ul class="c-sidebar__list pc">
      <?php
        $param_cat = array(
          'post_type'                => 'topics',
          'orderby'                  => 'id',
          'order'                    => 'asc',
          'hide_empty'               => 0,
          'taxonomy'                 => 'topicscat',
          'parent'                  => 0,
          'pad_counts'               => true 
        );
        $categories = get_categories($param_cat);
        $i = 0;
        foreach ($categories as $itemcat) :
        ?>
      <li><a href="<?php echo get_term_link($itemcat->slug,'topicscat'); ?>"><?php echo $itemcat->name; ?>(<?php echo ($itemcat->count); ?>)</a></li>
      <?php endforeach; ?>
    </ul>
    <ul class="c-sidebar__list c-sidebar__list--select sp">
      <li class="is-select c-sidebar__item">選択してください</li>
      <?php
        $param_cat = array(
          'post_type'                => 'topics',
          'orderby'                  => 'id',
          'order'                    => 'asc',
          'hide_empty'               => 0,
          'taxonomy'                 => 'topicscat',
          'parent'                  => 0,
          'pad_counts'               => true
        );
        $categories = get_categories($param_cat);
        $i = 0;
        foreach ($categories as $itemcat) :
        ?>
      <li><a href="<?php echo get_term_link($itemcat,'topicscat'); ?>"><?php echo $itemcat->name; ?>(<?php echo sprintf("%02d",$itemcat->count); ?>)</a></li>
      <?php endforeach; ?>
    </ul>
  </div>
<!-- ------------------------------------------ -->
<!-- ---------------case archive--------------- -->
<!-- ------------------------------------------ -->
  <?php
  $post_date = array(
    'post_type'                => 'topics',
    'home_url'                 => '',
    'have_count'               => true,
    'add_zero_in_month'        => true,
    'add_zero_in_count'        => true
  );
  $the_query = new WP_Query( $post_date );
  $totalpost = $the_query->found_posts;
  if($totalpost):
  ?>
  <div class="c-sidebar__article">
    <p class="c-sidebar__heading">Archive</p>
    <ul class="c-sidebar__list pc">
      <?php
        echo wp_post_type_archive(array('post_type' => 'topics','have_count' => true));
      ?>
    </ul>
    <ul class="c-sidebar__list c-sidebar__list--select1 sp">
      <li class="is-select1 c-sidebar__item1">選択してください</li>
      <?php
        echo wp_post_type_archive(array('post_type' => 'topics','have_count' => true));
      ?>
    </ul>
  </div>
  <?php endif; ?>
</div>