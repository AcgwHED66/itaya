<?php
$thisPageName = 'topics';
$title_sg = get_the_title();
$term = get_the_terms($singleID, 'topicscat');
$desPage = mb_substr(preg_replace('/\r\n|\n|\r/','',strip_tags($post->post_content)),0,120);
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/topics.min.css">
</head>
<body id="topics-details" class='topics-details'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
  <main>
    <div id="breadcrumb" class="breadcrumb">
      <div class="content">
        <ul>
          <li><a href="<?php echo APP_URL ?>"><img width="14" src="<?php echo APP_ASSETS ?>/img/common/icon/ico_breadcrumb.svg"></a></li>
          <li><a href="<?php echo APP_URL ?>topics/">トピックス</a></li>
          <li><span><?php echo wp_strip_all_tags($title_sg); ?></span></li>
        </ul>
      </div>
    </div>
    <div class="visual">
      <div class="content">
        <span>Topics</span>
        <h1>トピックス</h1>
      </div>
    </div>
    <div class="topics-inner">
      <div class="c-article">
        <div class="detail">
          <div class="detail-inner content cmsNews cmsContent">
            <div class="detail-cate">
              <span><?php echo get_the_date('Y.m.d'); ?></span>
               <?php if(isset($term) && $term) :
                  foreach($term as $cat) :
                ?>
                <p class="cat"><?php echo $cat->name; ?></p>
                <?php endforeach; endif; ?>
            </div>
            <h1 class="detail-ttl"><?php the_title(); ?></h1>
            <?php while ( have_rows( 'topics_detail' ) ) : the_row(); ?>
             <?php
                $imageThumb = get_sub_field('topics_image');
                    if ( $imageThumb ) { ?>
              <?php $thumb =  $imageThumb; ?>
              <?php } else { ?>
              <?php $thumb =  APP_ASSETS.'img/top/img_dummy_bg.png' ?>
            <?php } ?>
            <div class="detail-image" rel="js-lazy" data-src="<?php echo $thumb; ?>"></div>
            <?php endwhile; ?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
              <?php the_content();?>
            <?php endwhile; endif; ?>
          </div>
        </div>
        <?php
          $prev_post = get_previous_post();
          $next_post = get_next_post();
        ?>
        <div class="pageview">
          <?php if($prev_post || $next_post): ?>
            <ul class="pageview-list">
              <li class="pageview-item left">
                <?php if($prev_post): ?>
                <a href="<?php the_permalink($prev_post->ID); ?>">
                  <div class="previouspostslink"><span>Prev</span></div>
                  <div class="pageview-box">
                    <?php while ( have_rows( 'topics_detail' ) ) : the_row(); ?>
					            <?php
                      $imageThumb= get_sub_field('topics_image');
                      if ( $imageThumb ) { ?>
                      <?php $thumb =  $imageThumb; ?>
                      <?php } else { ?>
                      <?php $thumb =  APP_ASSETS.'img/top/img_topics03.jpg' ?>
                    <?php } ?>
                    <div class="pageview-img" data-src="<?php echo $thumb; ?>"></div>
                    <?php endwhile; ?>
                    <div class="pageview-info">
                      <div class="pageview-cate">
                        <span><?php echo get_the_date('Y.m.d', $prev_post); ?></span>
                        <?php if(isset($term) && $term) :
                          foreach($term as $itemcat) :
                        ?>
                        <span class="noti"><?php echo $itemcat->name; ?></span>
                        <?php endforeach; endif; ?>
                      </div>
                      <?php $title_prev = get_the_title($title_prev); ?>
                      <h3 class="pageview-ttl"><?php echo wp_strip_all_tags($title_prev); ?></h3>
                    </div>
                  </div>
                </a>
                <?php endif; ?>
              </li>
              <li class="pageview-item mid">
                <a href="<?php echo APP_URL ?>topics/">
                  <span>一覧</span>
                </a>
              </li>
              <li class="pageview-item right">
                <?php if($next_post): ?>
                <a href="<?php the_permalink($next_post->ID); ?>">
                  <div class="nextpostslink"><span>Next</span></div>
                  <div class="pageview-box">
                     <?php while ( have_rows( 'topics_detail' ) ) : the_row(); ?>
					          <?php
                      $imageThumb= get_sub_field('topics_image');
                      if ( $imageThumb ) { ?>
                      <?php $thumb =  $imageThumb; ?>
                      <?php } else { ?>
                      <?php $thumb =  APP_ASSETS.'img/top/img_topics03.jpg' ?>
                    <?php } ?>
                    <div class="pageview-img" data-src="<?php echo $thumb; ?>"></div>
                    <?php endwhile; ?>
                    <div class="pageview-info">
                      <div class="pageview-cate">
                        <span><?php echo get_the_date('Y.m.d', $next_post); ?></span>
                         <?php if(isset($term) && $term) :
                          foreach($term as $itemcat) :
                        ?>
                        <span class="noti"><?php echo $itemcat->name; ?></span>
                        <?php endforeach; endif; ?>
                      </div>
                      <?php $title_next = get_the_title($next_post); ?>
                      <h3 class="pageview-ttl"><?php echo wp_strip_all_tags($title_next); ?></h3>
                    </div>
                  </div>
                </a>
                <?php endif; ?>
              </li>
            </ul>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
<script src="<?php echo APP_ASSETS ?>js/page/topics.min.js"></script>
</body>
</html>