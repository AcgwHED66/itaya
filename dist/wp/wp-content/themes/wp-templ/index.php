<?php
$thisPageName = 'top';

$param_cat = array(
  'post_type'   => 'event',
  'parent'      => 0,
  'hide_empty'  => true,
  'taxonomy'    => 'eventcat',
);
$categories = get_categories($param_cat);

$param_work_cat = array(
  'post_type'   => 'work',
  'parent'      => 0,
  'hide_empty'  => true,
  'taxonomy'    => 'workcat',
);
$categories_work = get_categories($param_work_cat);

$param_review_cat = array(
  'post_type'   => 'interview',
  'parent'      => 0,
  'hide_empty'  => true,
  'taxonomy'    => 'reviews',
);
$categories_review = get_categories($param_review_cat);

$param_topics_cat = array(
  'post_type'   => 'topics',
  'parent'      => 0,
  'hide_empty'  => true,
  'taxonomy'    => 'topicscat',
);
$categories_topic = get_categories($param_topics_cat);

include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/top.min.css">
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>js/slick/slick.css">
</head>
<body id="top" class="top">
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>

<main id="container" class="clearfix">
  <div class="c-mv">
    <div class="c-mv__inner">
      <div class="c-mv__slider">
        <div class="c-mv__item">
          <img class="pc" src="<?php echo APP_ASSETS ?>img/top/img_mainvisual.jpg" alt="住まいのこと、ぜんぶ。">
          <img class="sp" src="<?php echo APP_ASSETS ?>img/top/img_mainvisual_sp.jpg" alt="住まいのこと、ぜんぶ。">
        </div>
        <div class="c-mv__item">
          <img class="pc" src="<?php echo APP_ASSETS ?>img/top/img_mainvisual_sample.jpg" alt="住まいのこと、ぜんぶ。">
          <img class="sp" src="<?php echo APP_ASSETS ?>img/top/img_mainvisual_sample_sp.jpg" alt="住まいのこと、ぜんぶ。">
        </div>
      </div>
      <div class="c-mv__content">
        <h1 class="title">住まいのこと、ぜんぶ。</h1>
        <h2 class="subtitle">用地仕入  ×  分譲住宅  ×  注文住宅  ×  リフォーム</h2>
        <p class="note">「住まい」を50年以上考え続けた<br class="sp">ノウハウが詰まった家づくり</p>
      </div>
    </div>
    <span class="icon"></span>
  </div>
  <!-- end mv -->
  <section class="c-search">
    <div class="c-search__content">
      <h2 class="c-search__heading">物件情報</h2>
      <div class="c-search__modal">
        <p class="c-search__ttl">エリアを指定して検索する</p>
        <p class="c-search__map" id="searchMap">エリアを選ぶ</p>
      </div>
      <div class="c-popup">
        <div class="c-popup__bg"></div>
        <div class="c-popup__cont">
          <div class="c-popup__close">
            <img src="<?php echo APP_ASSETS ?>img/top/ico_close_popup.svg" alt="エリアから探す">
          </div>
          <div class="c-popup__inner">
            <h3 class="c-popup__heading"><span>エリアから探す</span></h3>
            <div class="c-popup__list">
              <div class="c-popup__item">
                <h4 class="c-popup__ttl">岐阜エリア</h4>
                <div class="c-popup__box">
                  <label class="c-popup__label">下佐波 (02)
                    <input type="checkbox" name="area[]" data-num="2" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">上佐波 (01)
                    <input type="checkbox" name="area[]" data-num="1" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">琴塚 (03)
                    <input type="checkbox" name="area[]" data-num="3" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">北一色 (04)
                    <input type="checkbox" name="area[]" data-num="4" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">島田 (05)
                    <input type="checkbox" name="area[]" data-num="5" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">東鶉I (06)
                    <input type="checkbox" name="area[]" data-num="6" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">東鶉II (07)
                    <input type="checkbox" name="area[]" data-num="7" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">東鶉III (08)
                    <input type="checkbox" name="area[]" data-num="8" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">茜部新所 (00)
                    <input type="checkbox" name="area[]" data-num="" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">茜部菱野 (00)
                    <input type="checkbox" name="area[]" data-num="" value="">
                    <span class="checkmark"></span>
                  </label>
                </div>
              </div>
              <div class="c-popup__item">
                <h4 class="c-popup__ttl">大垣エリア</h4>
                <div class="c-popup__box">
                  <label class="c-popup__label">中川町 (02)
                    <input type="checkbox" name="area[]" data-num="2" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">領家町 (03)
                    <input type="checkbox" name="area[]" data-num="3" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">三津屋町 (04)
                    <input type="checkbox" name="area[]" data-num="4" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">友江 (05)
                    <input type="checkbox" name="area[]" data-num="5" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">南一色 (06)
                    <input type="checkbox" name="area[]" data-num="6" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">世安町 (07)
                    <input type="checkbox" name="area[]" data-num="7" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">昼飯 (08)
                    <input type="checkbox" name="area[]" data-num="8" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">東鶉III (09)
                    <input type="checkbox" name="area[]" data-num="9" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">茜部新所 (10)
                    <input type="checkbox" name="area[]" data-num="" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">茜部菱野 (22)
                    <input type="checkbox" name="area[]" data-num="" value="">
                    <span class="checkmark"></span>
                  </label>
                </div>
              </div>
              <div class="c-popup__item c-popup__item--prewidth">
                <h4 class="c-popup__ttl">各務原エリア</h4>
                <div class="c-popup__box">
                  <label class="c-popup__label">鵜沼各務原町Ⅱ (12)
                    <input type="checkbox" name="area[]" data-num="12" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">鵜沼各務原町Ⅲ (00)
                    <input type="checkbox" name="area[]" data-num="" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">那加西市場町 (10)
                    <input type="checkbox" name="area[]" data-num="10" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">那加新加納町 (20)
                    <input type="checkbox" name="area[]" data-num="20" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">那加巾下 (00)
                    <input type="checkbox" name="area[]" data-num="" value="">
                    <span class="checkmark"></span>
                  </label>
                </div>
              </div>
              <div class="c-popup__item">
                <h4 class="c-popup__ttl">岐南エリア</h4>
                <div class="c-popup__box">
                  <label class="c-popup__label">八剣北 (02)
                    <input type="checkbox" name="area[]" data-num="2" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">野中 (00)
                    <input type="checkbox" name="area[]" data-num="" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">若宮地 (05)
                    <input type="checkbox" name="area[]" data-num="5" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">三宅 (06)
                    <input type="checkbox" name="area[]" data-num="6" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">平成 (09)
                    <input type="checkbox" name="area[]" data-num="9" value="">
                    <span class="checkmark"></span>
                  </label>
                </div>
              </div>
              <div class="c-popup__item">
                <h4 class="c-popup__ttl">その他</h4>
                <div class="c-popup__box">
                  <label class="c-popup__label">羽島 (00)
                    <input type="checkbox" name="area[]" data-num="" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">瑞穂 (12)
                    <input type="checkbox" name="area[]" data-num="12" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">笠松 (25)
                    <input type="checkbox" name="area[]" data-num="25" value="">
                    <span class="checkmark"></span>
                  </label>
                  <label class="c-popup__label">愛知県大口町 (00)
                    <input type="checkbox" name="area[]" data-num="" value="">
                    <span class="checkmark"></span>
                  </label>
                </div>
              </div>
            </div>
            <div class="c-popup__bottom">
              <p class="c-popup__result">該当する物件<span class="total">000</span>件</p>
              <a href="<?php echo APP_URL ?>/estate/search/" class="c-popup__find"><span>この条件で<br class="sp">検索する</span></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="c-search__slider">
      <h3 class="c-search__subttl">人気エリアの好立地物件を<br class="sp"><span>ご紹介します !!</span></h3>
      <p class="c-search__decor">おすすめ物件</p>
      <div class="c-search__slick">
        <?php
          $the_query = new WP_Query( array(
              'post_type'      => 'estate',
              'posts_per_page' => 4,
              'orderby'        => 'date',
              'order'          => 'DESC',
              'post_status'    => 'publish',
          ) );
          if ( $the_query->have_posts() ):
              while ( $the_query->have_posts() ): $the_query->the_post(); ?>
                <?php while ( have_rows( 'estate_detail' ) ) : the_row(); ?>
                <div class="c-search__item">
                  <p class="c-search__note"><span><?php the_sub_field( 'estate_adress' ); ?></span><?php the_sub_field( 'estate_character' ); ?></p>
                  <div class="c-search__inner">
                    <div class="c-search__child"><p class="c-search__text"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></p></div>
                    <div class="c-search__area">
						<?php
                      $imageThumb = get_sub_field('estate_image');
                          if ( $imageThumb ) { ?>
                    <?php $thumb =  $imageThumb; ?>
                    <?php } else { ?>
                    <?php $thumb =  APP_ASSETS.'img/top/img_dummy_bg.png' ?>
                  <?php } ?>
                      <div class="c-search__img" rel="js-lazy" data-src="<?php echo $thumb; ?>"></div>
                      <div class="c-search__tbl">
                        <p class="c-search__number"><?php the_sub_field( 'estate_price' ); ?><span>万円</span></p>
                        <table>
                          <tbody>
                            <?php while ( have_rows( 'estate_table' ) ) : the_row(); ?>
                            <tr>
                              <th><?php the_sub_field( 'title_table' ); ?></th>
                              <td><?php the_sub_field( 'text_table' ); ?></td>
                            </tr>
                            <?php endwhile; ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <ul class="c-search__btn">
                      <li>
                        <a href="<?php the_permalink(); ?>">物件の詳細を見る</a>
                      </li>
                      <li>
                        <a href="<?php the_permalink(); ?>#">資料請求・見学会</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <?php endwhile; ?>
              <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
            <?php else: ?>
              <!-- <p class="text-center"></p> -->
        <?php endif; ?>
      </div>
    </div>
  </section>
  <!-- end search -->
  <section class="c-new">
    <div class="c-new__inner">
      <h2 class="c-new__heading"><span>NEW Property</span>新着物件</h2>
      <ul class="c-new__list">
        <?php
          $the_query = new WP_Query( array(
              'post_type'      => 'estate',
              'posts_per_page' => 6,
              'orderby'        => 'date',
              'order'          => 'DESC',
              'post_status'    => 'publish',
          ) );
          if ( $the_query->have_posts() ):
              while ( $the_query->have_posts() ): $the_query->the_post(); ?>
              <?php while ( have_rows( 'estate_detail' ) ) : the_row(); ?>
                <li class="c-new__item">
                  <div class="c-new__categ">
                    <p class="c-new__text"><?php the_sub_field( 'estate_adress' ); ?></p>
                    <p class="c-new__note"><?php the_sub_field( 'estate_character' ); ?></p>
                  </div>
                  <div class="c-new__box">
                    <h3 class="c-new__link"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>
                    <p class="c-new__prices"><?php the_sub_field( 'estate_price' ); ?><span>万円</span></p>
                    <div class="c-new__col">
						<?php
                      $imageThumb = get_sub_field('estate_image');
                          if ( $imageThumb ) { ?>
                    <?php $thumb =  $imageThumb; ?>
                    <?php } else { ?>
                    <?php $thumb =  APP_ASSETS.'img/top/img_dummy_bg.png' ?>
                  <?php } ?>
                      <div class="c-new__img" rel="js-lazy" data-src="<?php echo $thumb; ?>"></div>
                      <div class="c-new__tbl">
                        <table>
                          <tbody>
                            <?php while ( have_rows( 'estate_table' ) ) : the_row(); ?>
                            <tr>
                              <th><?php the_sub_field( 'title_table' ); ?></th>
                              <td><?php the_sub_field( 'text_table' ); ?></td>
                            </tr>
                            <?php endwhile; ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </li>
        <?php endwhile; ?>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
      </ul>
      <a href="<?php echo APP_URL ?>estate/" class="c-new__btn">物件情報一覧を見る</a>
      <?php else: ?>
    <?php endif; ?>
    </div>
  </section>
  <!-- end new -->
  <section class="c-event">
    <div class="c-event__inner content">
      <h2 class="c-event__heading"><span>Events</span>伊田屋の家づくりを体感できる<br class="sp"> イベント・見学会情報</h2>
      <div class="c-event__cont">
        <?php if($categories): ?>
          <?php
          foreach ($categories as $itemcat):
          $param = array(
            'posts_per_page' => '6',
            'post_type'      => 'event',
            'post_status'    => 'publish',
            'orderby'        => 'post_date',
            'order'          => 'DESC',
            'tax_query' => array(
              array(
                'taxonomy' => "eventcat",
                'field'    => 'slug',
                'terms'    => $itemcat->slug,
              )
            )
          );
          $wp_query = new WP_Query();
          $wp_query->query($param);
          if($wp_query->have_posts()):
          ?>
          <div class="c-event__col" id="<?php echo $itemcat->slug; ?>">
            <h3 class="c-event__ttl"><?php echo $itemcat->name; ?></h3>
            <!-- .lst event -->
            <ul class="c-event__list">
              <?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
              <li class="c-event__item">
                <?php while ( have_rows( 'event_detail' ) ) : the_row(); ?>
                <a href="<?php the_permalink(); ?>">
					<?php
                    $imageThumb= get_sub_field('event_image');
                    if ( $imageThumb ) { ?>
                    <?php $thumb =  $imageThumb; ?>
                    <?php } else { ?>
                    <?php $thumb =  APP_ASSETS.'img/top/img_dummy_bg.png' ?>
                  <?php } ?>
                  <div class="c-event__img" rel="js-lazy" data-src="<?php echo $thumb; ?>" ></div>
                  <div class="c-event__info">
                    <div class="c-event__categ pc">
                      <span><?php the_sub_field( 'event_cate' ); ?></span>
                      <small>開催日時</small>
                      <em><?php the_sub_field( 'event_time' ); ?>〜</em>
                      <!-- <?php the_sub_field( 'event_time_end' ); ?> -->
                    </div>
                    <p class="c-event__tag sp"><?php the_sub_field( 'event_cate' ); ?></p>
                    <h4 class="c-event__subttl"><?php the_title(); ?></h4>
                    <div class="c-event__categ sp">
                      <small>開催日時</small>
                      <em><?php the_sub_field( 'event_time' ); ?>〜</em>
                    </div>
                  </div>
                </a>
                  <?php endwhile; ?>
              </li>
              <?php endwhile; ?>
            </ul>
              <!-- <a href="<?php echo get_term_link($itemcat);?>" class="c-event__btn"><?php echo $itemcat->name; ?>のイベント一覧を見る</a> -->
              <a href="<?php echo APP_URL ?>event/<?php echo $itemcat->slug; ?>/" class="c-event__btn"><?php echo $itemcat->name; ?>のイベント一覧を見る</a>
            <!-- /.lst event -->
          </div>
          <?php
          endif; wp_reset_postdata();
          endforeach; ?>
          <!-- /.itemCat -->
        <?php endif; ?>
      </div>
    </div>
  </section>
  <!-- end event -->
  <section class="c-building">
    <h2 class="c-building__heading"><span>Building<br class="sp"> a house</span>伊田屋の家づくり</h2>
    <ul class="c-building__list">
      <li class="c-building__item">
        <div class="c-building__img">
          <img src="<?php echo APP_ASSETS ?>img/top/img_building01.jpg" alt="分譲住宅 Houses built for sale">
        </div>
        <div class="c-building__cont">
          <div class="c-building__box">
            <p class="c-building__decor"><span>01</span><span>Houses built<br>for sale</span></p>
            <div class="c-building__info">
              <h3 class="c-building__title">分譲住      宅</h3>
              <p class="c-building__text">Houses built for sale</p>
            </div>
            <p class="c-building__intro">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ<span class="sp">ストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</span></p>
            <a href="<?php echo APP_URL ?>/subdivision/" class="c-building__link pc"><span>詳しく見る</span></a>
          </div>
        </div>
      </li>
      <li class="c-building__item">
        <div class="c-building__cont">
          <div class="c-building__box">
            <p class="c-building__decor"><span>02</span><span>Custom<br>home</span></p>
            <div class="c-building__info">
              <h3 class="c-building__title">注文住宅</h3>
              <p class="c-building__text">Custom home</p>
            </div>
            <p class="c-building__intro">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ<span class="sp">ストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</span></p>
          </div>
        </div>
        <div class="c-building__img">
          <img src="<?php echo APP_ASSETS ?>img/top/img_building02.jpg" alt="注文住宅 Custom home">
        </div>
      </li>
      <li class="c-building__item">
        <div class="c-building__img">
          <img src="<?php echo APP_ASSETS ?>img/top/img_building03.jpg" alt="リフォーム Remodeling">
        </div>
        <div class="c-building__cont">
          <div class="c-building__box">
            <p class="c-building__decor"><span>03</span><span>Remodeling</span></p>
            <div class="c-building__info">
              <h3 class="c-building__title">リフォーム</h3>
              <p class="c-building__text">Remodeling</p>
            </div>
            <p class="c-building__intro">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ<span class="sp">ストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</span></p>
          </div>
        </div>
      </li>
    </ul>
  </section>
  <!-- end building -->
  <section class="c-works">
    <div class="c-works__inner content">
      <h2 class="c-works__heading"><span>Works</span>施工事例</h2>
      <ul class="c-works__list">
        <?php if($categories_work): ?>

          <?php
          foreach ($categories_work as $itemcat):
          $param_work_cat = array(
            'posts_per_page' => '1',
            'post_type'      => 'work',
            'post_status'    => 'publish',
            'orderby'        => 'post_date',
            'order'          => 'DESC',
            'tax_query' => array(
              array(
                'taxonomy' => "workcat",
                'field'    => 'slug',
                'terms'    => $itemcat->slug,
              )
            )
          );
          $wp_query = new WP_Query();
          $wp_query->query($param_work_cat);
          if($wp_query->have_posts()):
          ?>
          <?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <?php while ( have_rows( 'work_detail' ) ) : the_row(); ?>
              <li class="c-works__item">
                <a href="<?php the_permalink(); ?>">
                  <?php
                      $imageThumb = get_sub_field('work_image');
                          if ( $imageThumb ) { ?>
                    <?php $thumb =  $imageThumb; ?>
                    <?php } else { ?>
                    <?php $thumb =  APP_ASSETS.'img/top/img_dummy_bg.png' ?>
                  <?php } ?>
                  <div class="c-works__img" rel="js-lazy" data-src="<?php echo $thumb; ?>"></div>
                  <div class="c-works__cate">
                    <span class="txt1"><?php echo $itemcat->name; ?></span>
                    <span class="txt2"><?php the_sub_field( 'work_infor' ); ?></span>
                  </div>
                  <p class="c-works__text"><?php the_title();?></p>
                  <p class="c-works__note"><?php the_sub_field( 'work_note' ); ?></p>
                </a>
              </li>
            <?php endwhile; ?>
          <?php endwhile; ?>
          <?php
          endif; wp_reset_postdata();
          endforeach; ?>
        <?php endif; ?>
      </ul>
      <a href="<?php echo APP_URL ?>works/" class="c-works__btn">施工実績を見る</a>
    </div>
  </section>
  <!-- end works -->
  <section class="c-interview">
    <div class="c-interview__inner content">
      <h2 class="c-interview__heading"><span>Interview</span>お客様インタビュー</h2>
      <ul class="c-interview__list pc">
        <?php if($categories_review): ?>
          <?php
          foreach ($categories_review as $itemcat):
          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
          $param_review_cat = array(
            'posts_per_page' => '3',
            'post_type'      => 'interview',
            'post_status'    => 'publish',
            'orderby'        => 'post_date',
            'order'          => 'DESC',
            'paged'            => $paged,
            'tax_query' => array(
              array(
                'taxonomy' => "reviews",
                'field'    => 'slug',
                'terms'    => $itemcat->slug,
              )
            )
          );
          $wp_query = new WP_Query();
          $wp_query->query($param_review_cat);
          if($wp_query->have_posts()):
          ?>
          <?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <?php while ( have_rows( 'reviews_detail' ) ) : the_row(); ?>
              <li class="c-interview__item">
                <a href="<?php the_permalink(); ?>">
				        	<?php
                      $imageThumb = get_sub_field('review_image');
                          if ( $imageThumb ) { ?>
                    <?php $thumb =  $imageThumb; ?>
                    <?php } else { ?>
                    <?php $thumb =  APP_ASSETS.'img/top/img_dummy_bg.png' ?>
                  <?php } ?>
                  <div class="c-interview__img" rel="js-lazy" data-src="<?php echo $thumb; ?>"></div>
                  <div class="c-interview__cate">
                    <span class="txt1"><?php echo $itemcat->name; ?></span>
                    <span class="txt2"><?php the_sub_field( 'review_infor' ); ?></span>
                  </div>
                  <p class="c-interview__text"><?php the_title();?></p>
                  <p class="c-interview__note"><?php the_sub_field( 'review_note' ); ?></p>
                </a>
              </li>
            <?php endwhile; ?>
          <?php endwhile; ?>
          <?php
          endif; wp_reset_postdata();
          endforeach; ?>
        <?php endif; ?>
      </ul>
      <ul class="c-interview__list c-interview__list--slider sp">
        <?php if($categories_review): ?>
          <?php
          foreach ($categories_review as $itemcat):
          $param_review_cat = array(
            'posts_per_page' => '4',
            'post_type'      => 'interview',
            'post_status'    => 'publish',
            'orderby'        => 'post_date',
            'order'          => 'DESC',
            'tax_query' => array(
              array(
                'taxonomy' => "reviews",
                'field'    => 'slug',
                'terms'    => $itemcat->slug,
              )
            )
          );
          $wp_query = new WP_Query();
          $wp_query->query($param_review_cat);
          if($wp_query->have_posts()):
          ?>
          <?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <?php while ( have_rows( 'reviews_detail' ) ) : the_row(); ?>
              <li class="c-interview__item">
                <a href="<?php the_permalink(); ?>">
					<?php
                      $imageThumb = get_sub_field('review_image');
                          if ( $imageThumb ) { ?>
                    <?php $thumb =  $imageThumb; ?>
                    <?php } else { ?>
                    <?php $thumb =  APP_ASSETS.'img/top/img_dummy_bg.png' ?>
                  <?php } ?>
                  <div class="c-interview__img" rel="js-lazy" data-src="<?php echo $thumb; ?>"></div>
                  <div class="c-interview__cate">
                    <span class="txt1"><?php echo $itemcat->name; ?></span>
                    <span class="txt2"><?php the_sub_field( 'review_infor' ); ?></span>
                  </div>
                  <p class="c-interview__text"><?php the_title();?></p>
                  <p class="c-interview__note"><?php the_sub_field( 'review_note' ); ?></p>
                </a>
              </li>
            <?php endwhile; ?>
          <?php endwhile; ?>
          <?php
          endif; wp_reset_postdata();
          endforeach; ?>
        <?php endif; ?>
      </ul>
      <a href="<?php echo APP_URL ?>reviews/" class="c-interview__btn">お客様インタビューを見る</a>
    </div>
  </section>
  <!-- end interview -->
  <section class="c-topics">
    <div class="c-topics__inner content">
      <div class="c-topics__left">
        <h2 class="c-topics__heading"><span>Topics</span>トピックス</h2>
        <a href="<?php echo APP_URL ?>topics/" class="c-topics__btn">トピックス一覧</a>
      </div>
      <div class="c-topics__right">
        <ul class="c-topics__list">
        <?php if($categories_topic): ?>
          <?php
          foreach ($categories_topic as $itemcat):
          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
          $param_topics_cat = array(
            'posts_per_page' => '4',
            'post_type'      => 'topics',
            'post_status'    => 'publish',
            'orderby'        => 'post_date',
            'order'          => 'DESC',
            'paged'            => $paged,
            'tax_query' => array(
              array(
                'taxonomy' => "topicscat",
                'field'    => 'slug',
                'terms'    => $itemcat->slug,
              )
            )
          );
          $wp_query = new WP_Query();
          $wp_query->query($param_topics_cat);
          if($wp_query->have_posts()):
          ?>
          <?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <?php while ( have_rows( 'topics_detail' ) ) : the_row(); ?>
              <li class="c-topics__item">
                <a href="<?php the_permalink(); ?>">
                <?php
                  $imageThumb= get_sub_field('topics_image');
                  if ( $imageThumb ) { ?>
                  <?php $thumb =  $imageThumb; ?>
                  <?php } else { ?>
                  <?php $thumb =  APP_ASSETS.'img/top/img_dummy_bg.png' ?>
                <?php } ?>
                  <div class="c-topics__img" rel="js-lazy" data-src="<?php echo $thumb; ?>"></div>
                  <div class="c-topics__ctn">
                    <span><?php the_time('Y.m.d') ?></span>
                    <p class="c-topics__cat"><?php echo $itemcat->name; ?></p>
                    <h3 class="c-topics__title"><?php the_title();?></h3>
                  </div>
                </a>
              </li>
            <?php endwhile; ?>
          <?php endwhile; ?>
          <?php
          endif; wp_reset_postdata();
          endforeach; ?>
        <?php endif; ?>
        </ul>
      </div>
    </div>
  </section>
  <!-- end topics -->
  <section class="c-seo">
    <div class="c-seo__area content">
      <h3 class="c-seo__ttl">ダミーSEOテキストタイトルダミーSEOテキストタイトルダミーSEOテキストタイトルダミーSEOテキストタイトルダミーSEOテキストタイトル</h3>
      <div class="c-seo__inner">
        <div class="c-seo__left">
          <p>ダミーSEOテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト<br>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
          <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテ<span class="sp">キストテキストテキストテキストテキストテキスト</span></p>
        </div>
        <div class="c-seo__right">
          <p>ダミーSEOテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト<br>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
          <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテ<span class="sp">キストテキストテキストテキストテキストテキスト</span></p>
        </div>
      </div>
      <a href="<?php echo APP_URL ?>" class="c-seo__link">テキストテキストテキスト</a>
    </div>
  </section>
  <!-- end seo -->
</main>
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
<script src="<?php echo APP_ASSETS; ?>/js/slick/slick.min.js"></script>
<script src="<?php echo APP_ASSETS; ?>/js/jquery.matchHeight-min.js"></script>
<script src="<?php echo APP_ASSETS ?>/js/page/top.min.js"></script>
</body>
</html>