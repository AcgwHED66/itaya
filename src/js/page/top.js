// $(function () {
  // console.log('alert');
// });
// ------------------------------------------
// mainvisual slider
// ------------------------------------------
$('.c-mv__slider').slick({
  draggable: true,
  autoplay: true,
  autoplaySpeed: 2500,
  arrows: false,
  dots: false,
  fade: true,
  speed: 2500,
  infinite: true,
  cssEase: 'ease-in-out',
  touchThreshold: 100
})
$('.c-search__slick').slick({
  dots: true,
  infinite: true,
  autoplaySpeed: 5000,
  slidesToShow: 1,
  autoplay: true,
  arrows: true,
})
$('.c-interview__list--slider').slick({
  dots: true,
  infinite: true,
  autoplaySpeed: 5000,
  slidesToShow: 1,
  autoplay: true,
  arrows: true,
})
// ------------------------------------------
// fc click search map
// ------------------------------------------
$('#searchMap').on('click', function(){
  $(".c-popup").toggleClass('is-modal');
    var curPosition = $(window).scrollTop();
    var curHeight = $("body").outerHeight();
  if ($('.c-popup').hasClass("is-modal")) {
    $("body").attr("data-position", curPosition);
    $("body").css({
      "top": "-" + curPosition + "px",
      "left": 0,
      "width": "100%",
      "height": curHeight + "px",
      "position": "fixed",
      "z-index": "-1",
      "touch-action": "none",
      "overflow-y": "auto",
    });
  } else {
    var $pos_position = $("body").attr("data-position");
    $("body").css({
        "touch-action":"auto",
        "overflow-x":"hidden",
        "overflow-y":"auto",
        "position":"static",
        "top":"auto",
        "height": "auto"
    });
    $("html,body").scrollTop(Math.abs(Number($pos_position)));
  }
})
// ------------------------------------------
// fc close modal search
// ------------------------------------------
$('.c-popup__close').on('click', function(){
  $(".c-popup").toggleClass('is-modal');
    var curPosition = $(window).scrollTop();
    var curHeight = $("body").outerHeight();
  if ($('.popup').hasClass("is-modal")) {
      $("body").attr("data-position",curPosition);
      $("body").css({
          "top" : "-"+curPosition+"px",
          "left" : 0,
          "width" : "100%",
          "height" : curHeight+"px",
          "position" : "fixed",
          "z-index" : "-1",
          "touch-action" : "none",
          "overflow-y":"auto",
      });
  } else {
      var $pos_position = $("body").attr("data-position");
      $("body").css({
          "touch-action":"auto",
          "overflow-x":"hidden",
          "overflow-y":"auto",
          "position":"static",
          "top":"auto",
          "height": "auto"
      });
      $("html,body").scrollTop(Math.abs(Number($pos_position)));
  }
});
// ------------------------------------------
// ------------------------------------------
$(document).ready(function () {
  // fc popup click checkbox
  $(".c-popup__label input:checkbox").change(function() {
      var number = 0;
      $(".c-popup__label input:checkbox:checked").each(function() {
          number += isNaN(parseInt($(this).attr("data-num"))) ? 0 : parseInt($(this).attr("data-num"))
      }),
      $(".c-popup__result .total").text(number)
  })
  // fc popup click checkbox
  // $('.search-child').matchHeight({});
  // fc data-src | bg-image
})
// ------------------------------------------
// ------------------------------------------
$(window).on("load", function () {
   setTimeout(function() {
      $(".header").addClass("views")
  }, 700)
  setTimeout(function() {
      $(".c-mv__slider").addClass("views")
  }, 900)
  setTimeout(function () {
      $(".c-mv__content").addClass("views")
  }, 300)
  setTimeout(function() {
      $(".icon").addClass("views")
  }, 900)
  setTimeout(function() {
      $(".c-search").addClass("views")
  }, 900)
});
// --------------------------------------------------------------