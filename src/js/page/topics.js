// ---------------------------------------------------
// ---------------------------------------------------
$(function () {
    $(".c-sidebar__list--select").on("click", ".is-select", function () {
        $(this).closest(".c-sidebar__list--select").children('li:not(.is-select)').toggle();
    });
    var allOptions = $(".c-sidebar__list--select").children('li:not(.is-select)');
    $(".c-sidebar__list--select").on("click", "li:not(.is-select)", function() {
        allOptions.removeClass('selected');
        $(this).addClass('selected');
        $(".c-sidebar__list--select").children('.is-select').html($(this).html());
        allOptions.toggle();
    });
});
// ---------------------------------------------------
// ---------------------------------------------------
$(function () {
    $(".c-sidebar__list--select1").on("click", ".is-select1", function() {
        $(this).closest(".c-sidebar__list--select1").children('li:not(.is-select1)').toggle();
    });
    var allOptions1 = $(".c-sidebar__list--select1").children('li:not(.is-select1)');
    $(".c-sidebar__list--select1").on("click", "li:not(.is-select1)", function() {
        allOptions1.removeClass('selected1');
        $(this).addClass('selected1');
        $(".c-sidebar__list--select1").children('.is-select1').html($(this).html());
        allOptions1.toggle();
    });
});

