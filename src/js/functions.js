jQuery(document).ready(function($) {
  // Add rel 'noopener' for a with _blank target to prevent security rish
  $('a')
  .filter('[href^="http"], [href^="//"]')
  .not('[href*="' + window.location.host + '"]')
  .attr('rel', 'noopener')
  .not('.trusted')
  .attr('target', '_blank');
});

// Resize thumbnail box height
function thumbImg(dom, ratio_pc, ratio_sp) {
  if (dom && ratio_pc && ratio_sp && $(dom).length > 0) {
    var ratio = ratio_pc;
    if ($(window).width() < 768) {
      ratio = ratio_sp;
    }
    var h = Math.round($(dom).width() / ratio);
    $(dom).css('height',h).addClass('loaded');
  } else {
    return false;
  }
}

function IEdetection() {
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older, return version number
    return ('IE ' + parseInt(ua.substring(
    msie + 5, ua.indexOf('.', msie)), 10));
  }
  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11, return version number
    var rv = ua.indexOf('rv:');
    return ('IE ' + parseInt(ua.substring(
    rv + 3, ua.indexOf('.', rv)), 10));
  }
  var edge = ua.indexOf('Edge/');
  if (edge > 0) {
    //Edge (IE 12+), return version number
    return ('IE ' + parseInt(ua.substring(
    edge + 5, ua.indexOf('.', edge)), 10));
  }
  // User uses other browser
  return ('Not IE');
}

function GoogleFontLoader() {
  WebFontConfig = {
    google: { families:['Playfair+Display:400','Gentium+Basic:400','Noto+Sans+JP:400,700','Sawarabi+Mincho&display=swap&subset=japanese','Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700']},
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })();
}
// -------------------------------------------------------------
var list = document.querySelectorAll("div[data-src]");
  for (var i = 0; i < list.length; i++) {
    var url = list[i].getAttribute('data-src');
    list[i].style.backgroundImage="url('" + url + "')";
  }
